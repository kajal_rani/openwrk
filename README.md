# OpenWrks - OpenWrks NodeJS Wrapper
Basic Nodejs APIs OpenWrks with Eslint, babel (ECMA 8) and swagger implemented

###Setup instructions:
Once you have downloaded/cloned project from git repository please do the following:

#Following are the Guidelines:
1.Please don't forget to add code inside .env file from .env-template
2.Copy the format from .env-template file to .env and replace original values with "YOUR_VALUE_GOES_HERE" into .env file otherwise nodejs server will give errors while running the project instance on server


###Run instructions:

```sh
$ cd project-root
$ npm install
$ npm run start
```
###API guidelines

app.js in entry point of NodeJS wrapper.

###Api End Points
Please refer the swagger documentation for api descriptions.
http://ip:port/documentations/
Example : http://localhost:3000/documentations/


