'use strict';
import nodemailer from "nodemailer";
exports.sendPswResetEmail = (req,res,email,token,outputJson)=>{
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: process.env.EMAIL_HOST,
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: process.env.EMAIL_USER, // generated ethereal user
            pass: process.env.EMAIL_PASS // generated ethereal password
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from:process.env.EMAIL_FROM , // sender address
        to: email, // list of receivers
        subject: 'Password Reset Token', 
        html: 'Hello ' + email + ",<p>Please refer the attached password reset token for changing the password. Your token will expire after 24 hours</p><p><b>Password Reset Token : </b>"+ token + "</p>" + "" // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
        }
        else{
            console.log('Message sent: %s', info);            
        }
        res.end();
    });
};