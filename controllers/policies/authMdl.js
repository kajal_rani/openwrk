'use strict';
import tokenDbObj from "./../schemas/tokens/authTokens";
import results from "./../../config/results";
import constants from "./../../config/messages";


/*
 * authMdl.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Openwrks validateToken 
 * @date        :: 8 January 2019
 * @description :: This function is used to check all the private api for authentication valid token.
 *
 *
 */
exports.validateToken = (req, res, next) => {
    let outputJson = {};
    try {
        if (
            req.header &&
            req.header("Authorization") &&
            req.header("Authorization").split(' ')[0] === 'Bearer'
        ) {
            const apiToken = req.header("Authorization").split(' ')[1];
            tokenDbObj.findOne({
                token: apiToken,
                isDeleted: false,
                isExpired: false
            }).populate('loginId','_id email isDeleted isApproved').lean().exec((error, token) => {
                if (error) {
                    // mongodb ubuknown error
                    outputJson.status = constants.httpResponseCodes.failure;
                    outputJson.message = constants.messages.authentication.valTknunknownErr;
                    results.sendResponse(req, res, outputJson);
                } else if (!token) {
                    // token not found
                    outputJson.status = constants.httpResponseCodes.failure;
                    outputJson.message = constants.messages.authentication.apiTokenInvalidErr;
                    results.sendResponse(req, res, outputJson);
                } else if (token.isExpired) {
                    //token is expired
                    outputJson.status = constants.httpResponseCodes.failure;
                    outputJson.message = constants.messages.authentication.apiTokenExpiredErr;
                    results.sendResponse(req, res, outputJson);
                } else {
                    //success
                    req.user = token;
                    next();
                }

            });
        } else {
            //Authorization Header token is missing
            outputJson.status = constants.httpResponseCodes.failure;
            outputJson.message = constants.messages.authentication.apiTokenReqErr;
            results.sendResponse(req, res, outputJson);
        }
    } catch (error) {
        // unknown error
        outputJson.status = constants.httpResponseCodes.failure;
        outputJson.message = constants.messages.authentication.valTknunknownErr;
        results.sendResponse(req, res, outputJson);
    }
};