'use strict';
import loginDbObj from "./../schemas/login/logins";
import tokenDbObj from "./../schemas/tokens/authTokens";
import results from "./../../config/results";
import constants from "./../../config/messages";
const logger = require('../../config/logger')(module);
/*
 * App.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Openwrks Authenticate
 * @description :: Policies/auth.js Module dependencies.
 * @Company     :: Trantor Inc.
 * @Date        :: 8 January 2019
 */

exports.authenticate = (req, res) => {
    let outputJson = {};
    try {
        logger.info('authenticate is called');
        loginDbObj.findOne({
            email: req.body.email.toLowerCase(),
            isDeleted: false
        }).exec((err, user) => {
            if (err) {
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.loginUnknownErr;
                results.sendResponse(req, res, outputJson);
            } else if (!user) {
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.actNotFoundErr;
                results.sendResponse(req, res, outputJson);
            }
            //check if password is valid
            else if (!user.validPassword(req.body.password)) {
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.passswordReqErr;
                results.sendResponse(req, res, outputJson);
            }
            //check if account is approved by SA
            else if (!user.isApproved) {
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.acctApprovedErr;
                results.sendResponse(req, res, outputJson);
            }
            //check if account is blocked or not by SA
            else if (user.isBlocked) {
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.acctBlockedErr;
                results.sendResponse(req, res, outputJson);
            } else {
                //we are good to go generate auth token and save to user and send back to UI
                grantTokenAccess(req, res, outputJson, user);
            }

        });
    } catch (error) {
        logger.error(`authenticate - Catch : ${error}`);
        // console.log(error);
        outputJson.status = constants.httpResponseCodes.failure;
        outputJson.message = constants.messages.authentication.loginUnknownErr;
        results.sendResponse(req, res, outputJson);
    }
};


var grantTokenAccess = (req, res, outputJson, user) => {
    logger.info('grantTokenAccess is called');
    try {
        user.apiToken = user.generateAuthToken(user.email);
        let currentDate = new Date();
        let token = new tokenDbObj({
            loginId: user._id,
            token: user.apiToken
        });
        token.save((error, save) => {
            if (error) {
                // console.log("error:", error);
                logger.error(`grantTokenAccess - Error : ${error}`);
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.loginUnknownErr;
                results.sendResponse(req, res, outputJson);
            } else {
                logger.info(`grantTokenAccess - Data is saved`)
                outputJson.status = constants.httpResponseCodes.success;
                outputJson.message = constants.messages.authentication.success;
                outputJson.warning = constants.messages.authentication.warningMsg;
                outputJson.user = __.pick(user, "_id", "email", "apiToken");
                results.sendResponse(req, res, outputJson);
            }
        });
    } catch (err) {
        logger.error(`grantTokenAccess - Catch : ${err}`);
        // console.log("Err:", err);
        outputJson.status = constants.httpResponseCodes.failure;
        outputJson.message = constants.messages.authentication.loginUnknownErr;
        results.sendResponse(req, res, outputJson);
    }
};

exports.register = (req, res, next) => {
    let outputJson = {};
    logger.info('register is called');
    try {
        req.body.password = new loginDbObj().generateHash(req.body.password);
        loginDbObj.findOne({
            email: req.body.email.toLowerCase()
        }).lean().exec((err, user) => {
            if (err) {
                logger.error(`register - Error : ${err}`);
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.loginUnknownErr;
                results.sendResponse(req, res, outputJson);
            } else if (user) {
                logger.info(`register - user : ${JSON.stringify(user)}`);
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.usrnameUniqueErr;
                results.sendResponse(req, res, outputJson);
            } else {
                logger.info(`register - new User`);
                saveNewUser(req, res, outputJson);
            }
        });
    } catch (err) {
        // console.log("Error:", err)
        logger.error(`register - Catch : ${err}`);
        outputJson.status = constants.httpResponseCodes.failure;
        outputJson.message = constants.messages.register.registerUnknownErr;
        results.sendResponse(req, res, outputJson);
    }
};

var saveNewUser = (req, res, outputJson) => {
    logger.info('saveNewUser is called');
    try {
        let userObj = new loginDbObj(req.body);
        userObj.save((error, user) => {
            if (error) {
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.register.registerUnknownErr;
            } else if (!user) {
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.register.registerUnknownErr;
            } else {
                outputJson.status = constants.httpResponseCodes.success;
                outputJson.message = constants.messages.register.success;
            }
            results.sendResponse(req, res, outputJson);
        });
    } catch (err) {
        logger.error(`logout - Catch : ${err}`);
        outputJson.status = constants.httpResponseCodes.failure;
        outputJson.message = constants.messages.register.registerUnknownErr;
        results.sendResponse(req, res, outputJson);
    }
};


exports.logout = (req, res) => {
    logger.info('logout is called');
    let outputJson = {};
    try {
        tokenDbObj.findOneAndUpdate({
            token: req.user.token,
            isDeleted: false,
            isExpired: false
        }, {
                $set: {
                    isExpired: true,
                    loggedOutOn: new Date()
                }
            }, {
                returnNewDocument: true
            }).lean().exec((error, token) => {
                if (error) {
                    outputJson.status = constants.httpResponseCodes.failure;
                    outputJson.message = constants.messages.authentication.loginUnknownErr;

                } else if (!token) {
                    outputJson.status = constants.httpResponseCodes.failure;
                    outputJson.message = constants.messages.authentication.apiTokenInvalidErr;
                } else {
                    outputJson.status = constants.httpResponseCodes.success;
                    outputJson.message = constants.messages.authentication.logoutMsg;
                }
                results.sendResponse(req, res, outputJson);
            });
    } catch (err) {
        logger.error(`logout - Catch : ${err}`);
        outputJson.status = constants.httpResponseCodes.failure;
        outputJson.message = constants.messages.authentication.loginUnknownErr;
        results.sendResponse(req, res, outputJson);
    }
};