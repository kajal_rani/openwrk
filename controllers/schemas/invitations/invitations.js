'use strict';
/*
 * Users.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Customers Model
 * @date        :: 8 January 2019
 * @description :: This schema represents the invitations meta data information.
 * 
 *
 */

import mongoose from 'mongoose';
import constants from "./../../../config/messages";
const invitationScehma = new mongoose.Schema({
    "invitationId":{
        type:String,
        required:true,
        unique:true
    },
    "userId":{
        type:String, //reference id of openwrks environment
        required:true,
        ref:"customers" 
    },
    "customerReference":{
        type:String,
        required:true,
        ref:"customers"
    },
    "flowUrl":{
        type:String,
        required:true
    },
    "expirationDate":{
        type:Date,
        required:true
    },
    "status":{
        type:String,
        default:"Pending",
        enum:["Pending","Authorised","Revoked","Expired"]
    },
    "isDeleted": {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true,
    collection: 'invitations'
}).index({
    invitationId: 1,
    isDeleted: 1
});

module.exports = mongoose.model("invitations", invitationScehma);