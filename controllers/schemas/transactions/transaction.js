import mongoose from 'mongoose';

let schema = mongoose.Schema({
    accountId : {
        type : String,
        ref : 'Account',
        required : true
    },
    "transactionId":  {
        type : String,
        default : null
    },
    "amount":  {
        type : Number,
        default : 0
    },
    "description":  {
        type : String,
        default : null
    },
    "currency":  {
        type : String,
        default : null
    },
    "merchantName":  {
        type : String,
        default : null
    },
    "merchantCategoryCode":  {
        type : String,
        default : null
    },
    "timestamp":  {
        type : Date,
        default : null
    },
    "type":  {
        type : String,
        default : null
    },
    "category":  {
        type : String,
        default : null
    },
    "runningBalance":  {
        type : Number,
        default : 0
    },
    "status":  {
        type : String,
        default : null
    },
    "biz_id" : {
        type : String,
        default : null
    },
    "lead_id" : {
        type : String,
        default : null
    }
},{timestamps :  true});

module.exports = mongoose.model('Transaction', schema);