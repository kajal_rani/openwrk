'use strict';
/*
 * Users.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Login Model
 * @date        :: 8 January 2019
 * @description :: This schema represents the email, password login Ids.
 * 
 *
 */

import mongoose from 'mongoose';
import bcrypt from "bcrypt";
import constants from "./../../../config/messages";
const loginsScehma = new mongoose.Schema({
    "email": {
        type: String,
        required: constants.messages.authentication.usrnameReqErr,
        unique: true,
        lowercase: true
    },
    "password": {
        type: String,
        required: constants.messages.authentication.passswordReqErr
    },
    "isDeleted": {
        type: Boolean,
        default: false
    },
    "isBlocked": {
        type: Boolean,
        default: false
    },
    "isApproved":{
        type: Boolean,
        default: false
    }
}, {
    timestamps: true,
    collection: 'logins'
}).index({
    email: 1,
    isDeleted: 1
});

loginsScehma.methods.generateHash = function(password) {
    return bcrypt.hashSync(
        password,
        bcrypt.genSaltSync(Number(process.env.SALT_ROUNDS)),
        null
    );
};
// checking if password is valid
loginsScehma.methods.validPassword = function(password) {
    // console.log("password:",password);
    // console.log("this.password:",this.password);
    return bcrypt.compareSync(password, this.password);
};
// generate auth_token
loginsScehma.methods.generateAuthToken = function(username) {
    return bcrypt.hashSync(
        `${username}${new Date().getTime()}`,
        bcrypt.genSaltSync(Number(process.env.SALT_ROUNDS)),
        null
    );
};
module.exports = mongoose.model("logins", loginsScehma);