'use strict';
/*
 * Users.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Customers Model
 * @date        :: 8 January 2019
 * @description :: This schema represents the consents meta data information.
 * 
 *
 */

import mongoose from 'mongoose';
import constants from "./../../../config/messages";
const consentScehma = new mongoose.Schema({
    "consentId":{
        type:String,
        required:true
    },
    "userId":{
        type:String,
        required:true,
        ref:'customers'
    },
    "provider":{
        type:String,
        required:true
    },
    "flowUrl":{
        type:String,
        required:true
    },
    "expirationDate":{
        type:Date,
        required:true
    },
    "status":{
        type:String,
        required:true,
        enum:["Pending","Authorised","Revoked","Expired"]
    },
    "isDeleted": {
        type: Boolean,
        default: false
    },
    "bizId":{
        type:String,
        default:null
    },
    "leadId":{
        type:String,
        default:null
    },
    "createdDate":{
        type:Date,
        required:true
    },
    "updatedDate":{
        type:Date,
        required:true
    },
    "accounts":{
        type:mongoose.Schema.Types.Mixed,
        default:null
    }
}, {
    timestamps: true,
    collection: 'consents'
}).index({
    consentId: 1,
    isDeleted: 1
});

module.exports = mongoose.model("consents", consentScehma);