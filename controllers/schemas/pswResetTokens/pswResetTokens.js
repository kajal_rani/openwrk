'use strict';
/*
 * Users.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: AuthTokens Model
 * @date        :: 8 January 2019
 * @description :: This schema represents the authorization tokens to validate api requests.
 * @Company     :: 
 *
 */

import mongoose from 'mongoose';
import { messages } from "./../../../config/messages";
const pswResetTokensScehma = new mongoose.Schema({
    "loginId": {
        type: mongoose.Schema.Types.ObjectId,
        ref: "logins"
    },
    "token": {
        type: String,
        required: messages.authentication.tokenReqErr
    },
    "isDeleted": {
        type: Boolean,
        default: false
    },
    "isExpired": {
        type: Boolean,
        default: false
    },
    "expiredOn": {
        type: Date,
        default: null
    }
}, {
    timestamps: true,
    collection: 'pswResetTokens'
}).index({
    loginId: 1,
    token: 1
});
module.exports = mongoose.model("pswResetTokens", pswResetTokensScehma);