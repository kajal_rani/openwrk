import mongoose from 'mongoose';

let schema = mongoose.Schema({
    "userId" : {
        type : String,
        ref : "User"
    },
    "accountId": {
        type: String,
        required: true,
        index: true
    },
    "accountNumber": {
        type: String,
        required: true
    },
    "sortCode": {
        type: String,
        required: true
    },
    "accountName": {
        type: String,
        required: true
    },
    "iban": {
        type: String,
        required: true
    },
    "provider": {
        type: String,
        required: true
    },
    "balance": {
        type: Number,
        required: true
    },
    "currency": {
        type: String,
        required: true
    },
    "collectedDate": {
        type: Date,
        required: true
    },
    "consentStatus": {
        type: String,
        required: true
    },
    "biz_id" : {
        type : String,
        default : null
    },
    "lead_id" : {
        type : String,
        default : null
    }
},{timestamps : true});

module.exports = mongoose.model('Account', schema);