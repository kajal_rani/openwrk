'use strict';
/*
 * Users.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Customers Model
 * @date        :: 8 January 2019
 * @description :: This schema represents the customers meta data information.
 * 
 *
 */

import mongoose from 'mongoose';
import constants from "./../../../config/messages";
const customersScehma = new mongoose.Schema({
    "email": {
        type: String,
        required: constants.messages.authentication.usrnameReqErr,
        lowercase: true
    },
    "userId":{
        type:String //reference id of openwrks environment 
    },
    "firstname": {
        type: String,
        required:true
    },
    "surname": {
        type: String,
        required:true
    },
    "businessName": {
        type: String,
        required:true
    },
    "merchantId":{
        type:String,
        unique:true
    },
    "isDeleted": {
        type: Boolean,
        default: false
    },
    "bizId":{
        type:String,
        default:null
    },
    "leadId":{
        type:String,
        default:null
    }
}, {
    timestamps: true,
    collection: 'customers'
}).index({
    email: 1,
    merchantId:1,
    isDeleted: 1
});

module.exports = mongoose.model("customers", customersScehma);