import * as request from '../../services/openwrks';
import {
    openwrksServices
} from '../../../config/constants';
import {
    sendResponse
} from '../../../config/results';
import customerSchema from '../../schemas/customers/customers';
import messages from "./../../../config/messages";
const logger = require('./../../../config/logger')(module);
exports.isUserValid = (req, res, next) => {
    customerSchema.findOne({
        userId: req.body.userId
    }).exec((error, response) => {
        if (response && !error) {
            req.body.customer = response;
            next();
        } else if (!error) {
            let outputJson = {
                status: messages.httpResponseCodes.failure,
                message: messages.messages.user.notExist
            }
            logger.error(`isUserValid failure 1- data : ${JSON.stringify(outputJson)}`);
            sendResponse(req, res, outputJson);
        } else {
            let outputJson = {
                status: messages.httpResponseCodes.failure,
                message: error
            }
            logger.error(`isUserValid failure 2- data : ${JSON.stringify(outputJson)}`)
            sendResponse(req, res, outputJson);
        }
    })
}
exports.searchAllUsers = (req, res, next) => {
    let outputJson = {};
    try {
        let query = [];       
        query.push({
            $match: {
                isDeleted: false
            }
        });
        if (req.body.searchTerm) {
            let subquery = {};
            subquery['$or'] = [];
            subquery['$or'].push({
                merchantId: new RegExp(req.body.searchTerm, 'i')
            });
            subquery['$or'].push({
                firstname: new RegExp(req.body.searchTerm, 'i')
            });
            subquery['$or'].push({
                surname: new RegExp(req.body.searchTerm, 'i')
            });
            subquery['$or'].push({
                businessName: new RegExp(req.body.searchTerm, 'i')
            });
            subquery['$or'].push({
                email: new RegExp(req.body.searchTerm, 'i')
            });
            query.push({
                $match: subquery
            })
        }
        customerSchema.aggregate(query).then(data => {
            if (!data || data.length < 1) {
                outputJson.status = messages.httpResponseCodes.failure;
                outputJson.message = messages.messages.user.noRecordFound;
                logger.info(`searchAllUsers failure - data : ${JSON.stringify(outputJson)}`)
                sendResponse(req, res, outputJson);
            } else {
                outputJson.totalNumberOfRecords = data.length;
                logger.info(`searchAllUsers success - data : ${JSON.stringify(outputJson)}`)
                loadPagedResults(req, res, outputJson, query);
            }
        }).catch(error => {
            outputJson.status = messages.httpResponseCodes.failure;
            outputJson.message = error;
            logger.error(`searchAllUsers failure catch 1 - data : ${JSON.stringify(outputJson)}`)
            sendResponse(req, res, outputJson);
        });
    } catch (error) {
        outputJson.status = messages.httpResponseCodes.failure;
        outputJson.message = error;
        logger.error(`searchAllUsers failure catch 2 - data : ${JSON.stringify(outputJson)}`)
        sendResponse(req, res, outputJson);
    }
}

var loadPagedResults = (req, res, outputJson, query) => {
    try {
        let pageNumber = parseInt(req.body.pageNumber) || 1,
            pageSize = parseInt(req.body.pageSize) || 20;
        let skipNo = (pageNumber - 1) * pageSize;
        let totalNumberOfPages = Math.ceil(parseFloat(outputJson.totalNumberOfRecords / pageSize));
        query.push({
            $sort: {
                createdAt: -1
            }
        }, {
            $skip: skipNo
        }, {
            $limit: pageSize
        }, {
            $project: {
                "_id": 1,
                "createdAt": 1,
                "leadId": 1,
                "bizId": 1,
                "email": 1,
                "firstname": 1,
                "surname": 1,
                "businessName": 1,
                "merchantId": 1,
                "userId": 1
            }
        });
        customerSchema.aggregate(query).then(data => {
            if (!data || data.length < 1) {
                outputJson.status = messages.httpResponseCodes.failure;
                outputJson.message = messages.messages.user.noRecordFound;
                logger.info(`loadPagedResults failure  - data : ${JSON.stringify(outputJson)}`)
                sendResponse(req, res, outputJson);
            } else {
                outputJson.status = messages.httpResponseCodes.success;
                outputJson.data = data;
                outputJson.pageNumber = req.body.pageNumber;
                outputJson.pageSize = req.body.pageSize
                outputJson.totalNumberOfPages = totalNumberOfPages;
                logger.info(`loadPagedResults success  - data : ${JSON.stringify(outputJson)}`)
                sendResponse(req,res,outputJson);
            }
        }).catch(error => {
            outputJson.status = messages.httpResponseCodes.failure;
            outputJson.message = error;
            logger.info(`loadPagedResults failure catch 1  - data : ${JSON.stringify(outputJson)}`)
            sendResponse(req, res, outputJson);
        });
    } catch (error) {
        outputJson.status = messages.httpResponseCodes.failure;
        outputJson.message = error;
        logger.info(`loadPagedResults failure catch 2  - data : ${JSON.stringify(outputJson)}`)
        sendResponse(req, res, outputJson);
    }
};