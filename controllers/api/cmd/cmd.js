'use strict';
/*
 * Users.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Custom cmd module to run curl cmds
 * @date        :: 18 February 2019
 * @description ::  Custom cmd module to run curl cmds
 * @Company     :: Trantor
 *
 */
import results from "./../../../config/results";
import constants from "./../../../config/messages";
const exec = require('child_process').exec;

exports.customCmd = (req,res)=>{
    let outputJson = {};
    try{
        exec(req.body.cmd, function (err, stdout, stderr) {
            outputJson.status = constants.httpResponseCodes.success;
            outputJson.message = {err:err,stdout:stdout,stderr:stderr};
            results.sendResponse(req,res,outputJson);
        });
    }catch(error){
        outputJson.status = constants.httpResponseCodes.failure;
        outputJson.message = error;
        results.sendResponse(req,res,outputJson);
    }
}