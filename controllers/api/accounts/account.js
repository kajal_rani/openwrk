import request from '../../services/openwrks';
import { openwrksServices } from '../../../config/constants';
import accountSchema from '../../schemas/accounts/account';
import results from "./../../../config/results";
import messages from "./../../../config/messages";
const logger = require('../../../config/logger')(module);

let getAllAccountsFromOP = (req, res, next) => {
    return new Promise((resolve, reject) => {
        try {
            let options = {
                method: 'GET',
                url: `${process.env.OPENWRKS_ENDPOINT}${openwrksServices.users}/${req.body.userId}${openwrksServices.accounts}`,
                accessToken: `${process.env.OPENWRKS_TOKEN}`
            }
            logger.info(`getAllAccountsFromOP - options : ${JSON.stringify(options)}`);
            request(options).then(opResponse => {
                let { response } = opResponse;
                logger.info(`getAllAccountsFromOP - response : ${JSON.stringify(response)}`);
                if (response && response.data) {
                    __.map(response.data, (account) => {
                        account.userId = req.body.userId;
                        accountSchema.findOneAndUpdate({ accountId: account.accountId }, account, { upsert: true }).exec((error, result) => {
                            console.log(error, result);
                        });
                    })
                    resolve(response)
                } else if (response) {
                    reject(response)
                }
            }).catch(error => {
                logger.error(`getAllAccountsFromOP - inner Error : ${JSON.stringify(error)}`);
                reject(error);
            })
        } catch (error) {
            logger.error(`getAllAccountsFromOP - outer Error : ${JSON.stringify(error)}`);
            reject(error);
        }
    })
}

exports.getAllAccounts = (req, res, next) => {
    logger.info(`getAllAccounts - inside`);
    getAllAccountsFromOP(req, res, next).then((response) => {
        if (response && response.data) {
            logger.info(`getAllAccounts - response : ${JSON.stringify(response.data)}`);
            let outputJson = {
                status: messages.httpResponseCodes.success,
                data: response.data
            }
            results.sendResponse(req, res, outputJson);
        } else {
            logger.error(`getAllAccounts - response : ${JSON.stringify(response)}`);
            let outputJson = {}
            outputJson.status = response.status ? response.status : messages.httpResponseCodes.failure;
            outputJson.message = response.message;
            outputJson.errorCode = response.errorCode;
            outputJson._link = response._link;
            results.sendResponse(req, res, outputJson);
        }
    }).catch(error => {
        logger.error(`getAllAccounts - Error : ${JSON.stringify(error)}`);
        let outputJson = {}
        outputJson.status = messages.httpResponseCodes.failure;
        outputJson.message = error;
        results.sendResponse(req, res, outputJson);
    })
}

exports.getAllAccountsFromOP = getAllAccountsFromOP;

exports.getAccount = (req, res, next) => {
    try {
        let options = {
            method: 'GET',
            url: `${process.env.OPENWRKS_ENDPOINT}/${req.body.userId}${openwrksServices.accounts}/${req.body.accountId}`
        }
        logger.info(`getAccount - options : ${JSON.stringify(options)}`);
        request(options).then(response => {
            logger.info(`getAccount - response : ${JSON.stringify(response)}`);
            response.userId = req.body.userId;
            let accountInstance = new accountSchema(response);
            accountInstance.save();
            let outputJson = {
                status: messages.httpResponseCodes.success,
                data: response
            }
            results.sendResponse(req, res, outputJson);
        }).catch(error => {
            logger.error(`getAccount - response : ${JSON.stringify(error)}`);
            let outputJson = {};
            outputJson.status = messages.httpResponseCodes.failure;
            outputJson.message = error;
            results.sendResponse(req, res, outputJson);
        })
    } catch (error) {
        logger.error(`getAccount - error : ${JSON.stringify(error)}`);
        let outputJson = {};
        outputJson.status = messages.httpResponseCodes.failure;
        outputJson.message = error;
        results.sendResponse(req, res, outputJson);
    }
}

exports.getAccountsFromDB = (req, res, next) => {
    logger.info(`getAccountsFromDB - started`);
    const validateDate = new Date();
    validateDate.setDate(validateDate.getDate() - validateUpTo);
    let query = {
        userId: req.body.userId,
        updatedAt: { $gte: validateDate },
    }
    if (req.body.accountId) {
        query['accountId'] = req.body.accountId
    }
    logger.info(`getAccountsFromDB - query : ${JSON.stringify(query)}`);
    accountSchema.find(query).exec((error, dbResponse) => {
        logger.info(`getAccountsFromDB - response : ${JSON.stringify(dbResponse)}`);
        if (dbResponse && dbResponse.length > 0) {
            let outputJson = {
                status: messages.httpResponseCodes.success,
                data: dbResponse
            }
            results.sendResponse(req, res, outputJson);
        }
        else {
            next();
        }
    })
}
