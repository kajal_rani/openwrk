'use strict';
import results from "./../../../config/results";
import messages from "./../../../config/messages";
import constants from "./../../../config/constants";
import customerDbObj from "./../../schemas/customers/customers";
import {
    check,
    validationResult,
    oneOf
} from 'express-validator/check';
const logger = require('./../../../config/logger')(module);
/*

 * consents.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Openwrks validations.js controllers
 * @description :: API/consents/validations.js Module dependencies.
 * @Company     :: Trantor Inc.
 * @Date        :: 5 February 2019
 */

exports.customConsentValidation = (req, res, next) => {
    let outputJson = {};
    outputJson.status = messages.httpResponseCodes.failure;
    if (!req.body.userId && !req.body.merchantId) {
        // Required parameters are required
        outputJson.message = messages.messages.invite.requiredParamErr;
        results.sendResponse(req, res, outputJson);
    } else if (req.body.merchantId) {
        //all other validation for firstname, sername, 
        if (!req.body.firstname) {
            outputJson.message = messages.messages.invite.firstnameReqErr;
            results.sendResponse(req, res, outputJson);
        } else if (!req.body.surname) {
            outputJson.message = messages.messages.invite.surnameReqErr;
            results.sendResponse(req, res, outputJson);
        } else if (!req.body.email) {
            outputJson.message = messages.messages.authentication.emailReqErr;
            results.sendResponse(req, res, outputJson);
        }/*  else if (!req.body.leadId) {
            outputJson.message = messages.messages.invite.leadIdReqErr;
            results.sendResponse(req, res, outputJson);
        } else if (!req.body.bizId) {
            outputJson.message = messages.messages.invite.bizIdReqErr;
            results.sendResponse(req, res, outputJson);
        } */ else if (!req.body.businessName) {
            outputJson.message = messages.messages.invite.businessNameReqErr;
            results.sendResponse(req, res, outputJson);
        } else {
            next();
        }
    } else if (!req.body.merchantId) {
        if (!req.body.userId) {
            outputJson.message = messages.messages.invite.userIdReqErr;
            logger.error(`customConsentValidation failure1 - data : ${JSON.stringify(outputJson)}`);
            results.sendResponse(req, res, outputJson);
        } else {
            customerDbObj.findOne({
                userId: req.body.userId
            }).then(data => {
                if (!data) {
                    outputJson.message = messages.messages.invite.invalidUserId;
                    logger.error(`customConsentValidation failure2 - data : ${JSON.stringify(outputJson)}`);
                    results.sendResponse(req, res, outputJson);
                } else {
                    next();
                }
            }).catch(error => {
                outputJson.message = messages.messages.invite.allDataErr;
                logger.error(`customConsentValidation failure catch 1- data : ${JSON.stringify(outputJson)}`);
                results.sendResponse(req, res, outputJson);
            });

        }
    } else {
        //Either userId or merchantId information provide
        outputJson.message = messages.messages.invite.allDataErr;
        logger.error(`customConsentValidation failure 3- data : ${JSON.stringify(outputJson)}`);
        results.sendResponse(req, res, outputJson);

    }
};