'use strict';
import results from "./../../../config/results";
import messages from "./../../../config/messages";
import constants from "./../../../config/constants";
import openwrksService from "./../../services/openwrks";
import customersDbObj from "./../../schemas/customers/customers";
import invitationDbObj from "./../../schemas/invitations/invitations";
import * as accountCrtl from "./../accounts/account"
import consentDbObj from "./../../schemas/consents/consents";
const logger = require('./../../../config/logger')(module);

/*

 * consents.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Openwrks consents.js controllers
 * @description :: API/consents/consents.js Module dependencies.
 * @Company     :: Trantor Inc.
 * @Date        :: 4 February 2019
 */

exports.checkCustomerInfo = (req, res, next) => {
   let outputJson = {};
   try {
      req.body.reqOptions = {
         url: `${process.env.OPENWRKS_ENDPOINT}${constants.openwrksServices.invite}`,
         method: 'POST',
         accessToken:`${process.env.OPENWRKS_TOKEN}`
      };
      if (req.body.userId) {
         // Old User 2nd bank connection or Expired link flow 
         customersDbObj.findOne({
            userId: req.body.userId,
            isDeleted: false
         }).then(data => {
            if (!data) {
               outputJson.status = messages.httpResponseCodes.failure;
               outputJson.message = messages.messages.invite.invalidUserId;
               logger.info(`checkCustomerInfo failure- data : ${JSON.stringify(outputJson)}`);
               results.sendResponse(req, res, outputJson);
            } else {
               req.body.reqOptions.body = {
                  "customerReference": data.merchantId,
                  "redirectUrl": constants.redirectionUrl
               };
               next();
            }
         }).catch(err => {
            outputJson.status = messages.httpResponseCodes.failure;
            outputJson.message = err;
            logger.error(`checkCustomerInfo failure catch- data : ${JSON.stringify(outputJson)}`);
            results.sendResponse(req, res, outputJson);
         });
      } else {
         // new customer flow
         customersDbObj.findOne({
                 merchantId: req.body.merchantId,
                 isDeleted: false
             }).then(data => {
            if (!data) {
               let metadata = {
                  email: req.body.email,
                  firstname: req.body.firstname ? req.body.firstname : null,
                  surname: req.body.surname ? req.body.surname : null,
                  businessName: req.body.businessName ? req.body.businessName : null,
                  merchantId: req.body.merchantId ? req.body.merchantId : null,
                  bizId: req.body.bizId ? req.body.bizId : null,
                  leadId: req.body.leadId ? req.body.leadId : null
               };
               let inputs = new customersDbObj(metadata)
               inputs.save().then(data => {
                  metadata.wrapperId = data._id;
                  req.body.reqOptions.body = {
                     "customerReference": data.merchantId,
                     "redirectUrl": constants.redirectionUrl,
                     "metaData": metadata
                  };
                  next();
               }).catch(err => {
                  outputJson.status = messages.httpResponseCodes.failure;
                  outputJson.message = err;
                  logger.error(`checkCustomerInfo failure catch 1- data : ${JSON.stringify(outputJson)}`);
                  results.sendResponse(req, res, outputJson);
               })
            } else {
               req.body.reqOptions.body = {
                  "customerReference": data.merchantId,
                  "redirectUrl": constants.redirectionUrl
               };
               next();
            }
         }).catch(err => {           
            outputJson.status = messages.httpResponseCodes.failure;
            outputJson.message = err;
            logger.error(`checkCustomerInfo failure catch 2 - data : ${JSON.stringify(outputJson)}`);
            results.sendResponse(req, res, outputJson);
         });
      }
   } catch (error) {
      outputJson.status = messages.httpResponseCodes.failure;
      outputJson.message = error;
      logger.error(`checkCustomerInfo final catch  - data : ${JSON.stringify(outputJson)}`);
      results.sendResponse(req, res, outputJson);
   }
}

exports.invite = (req, res, next) => {
   let outputJson = {};
   try {
      openwrksService(req.body.reqOptions).then(data => {
          if(!data.response.flowUrl){ 
            outputJson.status = messages.httpResponseCodes.failure;
            outputJson.message = data.response.message;
            outputJson.errorCode = data.response.errorCode ? data.response.errorCode :null;
            outputJson.code = data.response.code ? data.response.code :null;
            logger.info(`invite failure  - data : ${JSON.stringify(outputJson)}`);
            results.sendResponse(req, res, outputJson);
          }
          else{                      
            req.body.invitation = data.response;
            next();
          }          
         //update userId  of user        
         
      }).catch(err => {        
         outputJson.status = messages.httpResponseCodes.failure;
         outputJson.message = err;
         logger.error(`invite failure  catch 1- data : ${JSON.stringify(outputJson)}`);
         results.sendResponse(req, res, outputJson);
      })
   } catch (error) {
      outputJson.status = messages.httpResponseCodes.failure;
      outputJson.message = error;
      logger.error(`invite failure  catch 2- data : ${JSON.stringify(outputJson)}`);
      results.sendResponse(req, res, outputJson);
   }
};

exports.storeInvitationLog = (req, res, next) => {
   let outputJson = {};
   try {
      let log = new invitationDbObj(req.body.invitation);
      log.save(); //saving invitation log
      if (req.body.merchantId) {
         customersDbObj.update({
            merchantId: req.body.merchantId
         }, {
            $set: {
               userId: req.body.invitation.userId
            }
         }).then(data=>{
            console.log("data:",data);
         }).catch(error=>{
            console.log("Error:",error);
         });
      }
      outputJson.status = messages.httpResponseCodes.success;
      outputJson.data = req.body.invitation;
      logger.info(`storeInvitationLog success- data : ${JSON.stringify(outputJson)}`);
      results.sendResponse(req, res, outputJson);
   } catch (error) {
      console.log("catch error:",error);
      outputJson.status = messages.httpResponseCodes.failure;
      outputJson.message = error;
      logger.error(`storeInvitationLog failure- data : ${JSON.stringify(outputJson)}`);
      results.sendResponse(req, res, outputJson);
   }
};

exports.checkCustomerConsentStatus = (req, res, next) => {
    customersDbObj.findOne({ userId: req.body.userId }, { 'consentApproved': 1 }).exec((error, dbResponse) => {
        if (error) {
            let outputJson = {
                status: messages.httpResponseCodes.failure,
                message: error
            }
            logger.error(`checkCustomerConsentStatus failure- data : ${JSON.stringify(outputJson)}`);
            results.sendResponse(req, res, outputJson);
        } else if (dbResponse) {
            if (dbResponse.consentApproved) {
                next();
            } else {
                let outputJson = {
                    status: messages.httpResponseCodes.failure,
                    message: messages.consent.notApproved
                }
                logger.info(`checkCustomerConsentStatus failure 1- data : ${JSON.stringify(outputJson)}`);
                results.sendResponse(req, res, outputJson);
            }
        } else {
            let outputJson = {
                status: messages.httpResponseCodes.failure,
                message: messages.user.notExist
            }
            logger.info(`checkCustomerConsentStatus failure 2- data : ${JSON.stringify(outputJson)}`);
            results.sendResponse(req, res, outputJson);
        }
    })
}

/**
 * req.body = {userId, includeExpired(optional)}
 * GET https://api.openwrks.com/surface/v1/users/<userId>/consents
 */
exports.getAllConsentsAndAccounts = (req, res, next) => {
    try {
        Promise.all([
            getAllConsents(req, res, next),
            accountCrtl.getAllAccountsFromOP(req, res, next)
        ]).then(values => {
            let consents = values[0].data;
            // if(consents && consents.data){
            const outputJson = {
                status: messages.httpResponseCodes.success,
                data: consents
            }
            logger.info(`getAllConsentsAndAccounts successs 1- data : ${JSON.stringify(outputJson)}`);
            results.sendResponse(req, res, outputJson);
            // }
        }, response => {
            let outputJson = {}
            outputJson.status = response.status ? response.status : messages.httpResponseCodes.failure;
            outputJson.message = response.message;
            outputJson.errorCode = response.errorCode;
            outputJson._link = response._link;
            logger.info(`getAllConsentsAndAccounts successs 2- data : ${JSON.stringify(outputJson)}`);
            results.sendResponse(req, res, outputJson);
        }).catch(error => {
            let outputJson = {};
            outputJson.status = messages.httpResponseCodes.failure;
            outputJson.message = error;
            logger.error(`getAllConsentsAndAccounts failure 1- data : ${JSON.stringify(outputJson)}`);
            results.sendResponse(req, res, outputJson);
        })
    } catch (error) {
        console.log(error);
        outputJson.status = messages.httpResponseCodes.failure;
        outputJson.message = error;
        logger.error(`getAllConsentsAndAccounts failure 2- data : ${JSON.stringify(outputJson)}`);
        results.sendResponse(req, res, outputJson);
    }
}

const getAllConsents = (req, res, next) => {
    return new Promise((resolve, reject) => {
        try {
            const options = {
                method: 'GET',
                url: `${process.env.OPENWRKS_ENDPOINT}${constants.openwrksServices.users}/${req.body.userId}${constants.openwrksServices.consents}`,
                accessToken: `${process.env.OPENWRKS_TOKEN}`
            }
            if(req.body.hasOwnProperty('includeExpired')){
                options.url += `?includeExpired=${req.body.includeExpired}`
            }
            openwrksService(options).then(opResponse => {
                let { response } = opResponse;
                if (response && response.data) {
                    __.map(response.data, (consent) => {
                        consent.userId = req.body.userId;
                        // let consentInstance = new consentDbObj(consent);
                        consentDbObj.findOneAndUpdate({ consentId: consent.consentId }, consent, { upsert: true }).exec((error, result) => {
                            console.log(error, result);
                        });
                    })
                    resolve(response);
                } else if (response) {
                    reject(response);
                }
            }).catch(opError => {
                reject(opError);
            })
        } catch (error) {
            reject(error);
        }
    })
}

/**
 * getAccountsForSpecificConsent
 * {consentId:""}
 */

exports.getAccountsForSpecificCosnent = (req, res, next) => {
    try {
        const query = [
            { $match: { "consentId": req.body.consentId } },
            { $unwind: "$accounts" },
            {
                $lookup: {
                    "localField": "accounts.accountId",
                    "foreignField": "accountId",
                    "from": "accounts",
                    "as": "accounts"
                }
            },
            { $unwind: "$accounts", },
            {
                $group: {
                    _id: "$consentId",
                    "accounts": { $addToSet: "$accounts" }
                }
            }
        ];
        consentDbObj.aggregate(query).exec((error, dbResponse) => {
            if (!error && dbResponse && dbResponse.length > 0) {
                const outputJson = {
                    status: messages.httpResponseCodes.success,
                    data: dbResponse[0].accounts ? dbResponse[0].accounts : []
                }
                logger.info(`getAccountsForSpecificCosnent success - data : ${JSON.stringify(outputJson)}`);
                results.sendResponse(req, res, outputJson);
            } else {
                let outputJson = {};
                outputJson.status = messages.httpResponseCodes.failure;
                outputJson.message = error;
                logger.error(`getAccountsForSpecificCosnent failure 1 - data : ${JSON.stringify(outputJson)}`);
                results.sendResponse(req, res, outputJson);
            }
        })
    } catch (error) {
        let outputJson = {};
        outputJson.status = messages.httpResponseCodes.failure;
        outputJson.message = error;
        logger.error(`getAccountsForSpecificCosnent failure catch 2 - data : ${JSON.stringify(outputJson)}`);
        results.sendResponse(req, res, outputJson);
    }
}

exports.checkCustomerConsentExist = (req, res, next) => {
    try {
        consentDbObj.findOne({ consentId: req.body.consentId }).exec((error, dbResponse) => {
            if (!error && dbResponse) {
                req.body.userId = dbResponse.userId;
                next();
            } else {
                let outputJson = {};
                outputJson.status = messages.httpResponseCodes.failure;
                outputJson.message = messages.messages.consent.noRecordFound;
                logger.error(`checkCustomerConsentExist failure - data : ${JSON.stringify(outputJson)}`);
                results.sendResponse(req, res, outputJson);
            }
        })
    } catch (error) {
        let outputJson = {};
        outputJson.status = messages.httpResponseCodes.failure;
        outputJson.message = error;
        logger.error(`checkCustomerConsentExist failure catch - data : ${JSON.stringify(outputJson)}`);
        results.sendResponse(req, res, outputJson);
    }
}

exports.reconsent = (req, res, next)=>{
    let outputJson = {};
    try {
        req.body.reqOptions = {
            url: `${process.env.OPENWRKS_ENDPOINT}${constants.openwrksServices.reconsent}`,
            method: 'POST',
            accessToken: `${process.env.OPENWRKS_TOKEN}`,
            body: {
                "consentId": req.body.consentId,
                "redirectUrl": constants.redirectionUrl
            }
        };
        next();
    } catch (error ){
        outputJson.status = messages.httpResponseCodes.failure;
        outputJson.message = error;
        logger.error(`reconsent failure catch - data : ${JSON.stringify(outputJson)}`);
        results.sendResponse(req, res, outputJson);
    };
};