import request from '../../services/openwrks';
import { openwrksServices } from '../../../config/constants';
import transactionSchema from '../../schemas/transactions/transaction';
import results from "./../../../config/results";
import messages from "./../../../config/messages";
import transactionService from './../../services/transaction'
const logger = require('../../../config/logger')(module);

exports.getAllTransactions = (req, res, next) => {
    try {
        let options = {
            method: "GET",
            url: `${process.env.OPENWRKS_ENDPOINT}${openwrksServices.users}/${req.body.userId}${openwrksServices.accounts}/${req.body.accountId}${openwrksServices.transactions}?`,
            accessToken: `${process.env.OPENWRKS_TOKEN}`
        }
        let dateFilter = false;
        if (req.body.from) {
            // call convert date to utc and iso format
            let utcDate = transactionService.convertDateToUTC(req.body.from);
            // utcDate = utcDate.substr(0, utcDate.length-1);
            // utcDate = utcDate + '+00:00'
            options.url += `from=${utcDate}&`
            dateFilter = true;
        }
        if (req.body.to) {
            // call convert date to utc and iso format
            let utcDate = transactionService.convertDateToUTC(req.body.to);
            // utcDate = utcDate.substr(0, utcDate.length-1);
            // utcDate = utcDate + '+00:00'
            options.url += `to=${utcDate}&`
            dateFilter = true;
        }
        if (req.body.hasOwnProperty('pageNumber')) {
            options.url += `page=${req.body.pageNumber}&`
        }
        if (req.body.hasOwnProperty('pageSize')) {
            options.url += `limit=${req.body.pageSize}&`
        }
        options.url = options.url.substring(0, options.url.length - 1);
        
        logger.info(`getAllTransactions - options : ${JSON.stringify(options)}`);
        //validate from and to date
        if (dateFilter && !transactionService.compareDate(req.body.from, req.body.to)) {
            let outputJson = {}
            outputJson.status = messages.httpResponseCodes.failure;
            outputJson.message = messages.messages.transaction.invalidDateFilter;
            results.sendResponse(req, res, outputJson);
        } else {
            request(options).then((opResponse) => {
                let { response } = opResponse;
                logger.info(`getAllTransactions - response : ${JSON.stringify(response)}`);
                if (response && response.data) {
                    response.data.map((transaction) => {
                        transaction.accountId = req.body.accountId;
                        let transactionObj = new transactionSchema(transaction)
                        transactionObj.save();
                    })
                    let outputJson = {
                        status: messages.httpResponseCodes.success,
                        data: response.data,
                        "totalNumberOfRecords": response._meta ? response._meta.totalNumberOfRecords : 0,
                        "totalNumberOfPages": response._meta ? response._meta.totalNumberOfPages : 0,
                        "pageNumber": response._meta ? response._meta.pageNumber : 0,
                        "pageSize": response._meta ? response._meta.pageSize : 0
                    }
                    results.sendResponse(req, res, outputJson);
                } else {
                    logger.error(`getAllTransactions - response : ${JSON.stringify(response)}`);
                    let outputJson = {}
                    outputJson.status = response.status ? response.status : messages.httpResponseCodes.failure;
                    outputJson.message = response.message;
                    outputJson.errorCode = response.errorCode;
                    outputJson._link = response._link;
                    results.sendResponse(req, res, outputJson);
                }
            }).catch(error => {
                logger.error(`getAllTransactions - error : ${JSON.stringify(error)}`);
                let outputJson = {}
                outputJson.status = messages.httpResponseCodes.failure;
                outputJson.message = error;
                results.sendResponse(req, res, outputJson);
            })
        }
    } catch (error) {
        logger.error(`getAllTransactions - error : ${JSON.stringify(error)}`);
        let outputJson = {}
        outputJson.status = messages.httpResponseCodes.failure;
        outputJson.message = error;
        results.sendResponse(req, res, outputJson);
    }
}