'use strict';
import loginDbObj from "./../../schemas/login/logins";
import results from "./../../../config/results";
import constants from "./../../../config/messages";
import mail from "./../../common/mail";
import pswResetdbObj from "./../../schemas/pswResetTokens/pswResetTokens";
const logger = require('../../../config/logger')(module);
/*
 * password.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Openwrks Authenticate
 * @description :: API/password/password.js Module dependencies.
 * @Company     :: Trantor Inc.
 * @Date        :: 8 January 2019
 */

var updatePassword = (data) => {
    logger.info(`updatePassword - with : ${JSON.stringify(data)}`);
    return new Promise((resolve, reject) => {
        try {
            loginDbObj.update({
                _id: data.loginId,
                isDeleted: false
            }, {
                    $set: {
                        password: new loginDbObj().generateHash(data.newPassword)
                    }
                }, (err, updt) => {
                    logger.info(`updatePassword - response : err : ${JSON.stringify(err)} -- updt : ${JSON.stringify(updt)}`);
                    if (err) {
                        resolve(err);
                    } else {
                        resolve(updt);
                    }
                });
        } catch (error) {
            logger.error(`updatePassword - error : ${JSON.stringify(error)}`);
            resolve(error);
        }
    });
};

exports.changePassword = (req, res) => {
    logger.info(`changePassword - called`);
    let outputJson = {};
    try {
        loginDbObj.findOne({
            _id: req.user.loginId,
            isDeleted: false
        }).exec((error, user) => {
            if (error) {
                logger.error(`changePassword - response : ${JSON.stringify(error)}`);
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.loginUnknownErr;
                results.sendResponse(req, res, outputJson);
            } else if (!user.validPassword(req.body.currentPassword)) {
                logger.info(`changePassword - response : password is not valid`);
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.changePassword.wrongCrtPswErr;
                results.sendResponse(req, res, outputJson);
            } else {
                updatePassword({
                    loginId: req.user.loginId,
                    newPassword: req.body.newPassword
                }).then(result => {
                    logger.info(`changePassword - updatePassword : result : ${JSON.stringify(result)}`);
                    outputJson.status = constants.httpResponseCodes.success;
                    outputJson.message = constants.messages.changePassword.success;
                    results.sendResponse(req, res, outputJson);
                }).catch(error => {
                    logger.error(`changePassword - updatePassword : error : ${JSON.stringify(error)}`);
                    outputJson.status = constants.httpResponseCodes.failure;
                    outputJson.message = constants.messages.authentication.loginUnknownErr;
                    results.sendResponse(req, res, outputJson);
                });
            }
        });
    } catch (error) {
        logger.error(`changePassword  - catch : ${JSON.stringify(error)}`);
        outputJson.status = constants.httpResponseCodes.failure;
        outputJson.message = constants.messages.authentication.loginUnknownErr;
        results.sendResponse(req, res, outputJson);
    }
};



exports.forgotPassword = (req, res) => {
    logger.info(`forgotPassword - called`);
    let outputJson = {};
    try {
        updatePassword({
            loginId: req.user.loginId,
            newPassword: req.body.newPassword
        }).then(result => {
            logger.info(`forgetPassword - updatePassword : response : ${JSON.stringify(result)}`);
            updaterstPswToken(req, res, outputJson);
        }).catch(error => {
            logger.error(`forgotPassword - updatePassword : catch : ${JSON.stringify(error)}`);
            outputJson.status = constants.httpResponseCodes.failure;
            outputJson.message = constants.messages.authentication.loginUnknownErr;
            results.sendResponse(req, res, outputJson);
        });
    } catch (error) {
        logger.error(`forgotPassword - catch : ${JSON.stringify(error)}`);
        outputJson.status = constants.httpResponseCodes.failure;
        outputJson.message = constants.messages.authentication.loginUnknownErr;
        results.sendResponse(req, res, outputJson);
    }
};

var updaterstPswToken = function (req, res, outputJson) {
    logger.info(`updaterstPswToken : called`);
    try {
        pswResetdbObj.update({
            token: req.body.token
        }, {
                $set: {
                    isExpired: true
                }
            }).then((result) => {
                logger.info(`updaterstPswToken - update : response : ${JSON.stringify(result)}`);
                outputJson.status = constants.httpResponseCodes.success;
                outputJson.message = constants.messages.changePassword.success;
                results.sendResponse(req, res, outputJson);
            }).catch((error) => {
                logger.error(`updaterstPswToken - update : error : ${JSON.stringify(error)}`);
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.loginUnknownErr;
                results.sendResponse(req, res, outputJson);
            });
    } catch (error) {
        logger.error(`updaterstPswToken - catch : ${JSON.stringify(error)}`);
        outputJson.status = constants.httpResponseCodes.failure;
        outputJson.message = constants.messages.authentication.loginUnknownErr;
        results.sendResponse(req, res, outputJson);
    }
};

exports.validatePswRstToken = (req, res, next) => {
    logger.info(`validatePswRstToken - called`);
    let outputJson = {};
    try {
        pswResetdbObj.findOne({
            token: req.body.token
        }).exec((err, token) => {
            if (err) {
                logger.error(`validatePswRstToken - findOne : error : ${JSON.stringify(err)}`);
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.loginUnknownErr;
                results.sendResponse(req, res, outputJson);
            } else if (!token) {
                logger.info(`validatePswRstToken - findOne : response - Invalid password reset token`)
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.resetPassword.invalidRstToken;
                results.sendResponse(req, res, outputJson);
            } else if (token.isExpired || new Date(token.expiredOn) < new Date()) {
                logger.info(`validatePswRstToken - findOne : response - token is expired`);
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.resetPassword.tokenExpiredErr;
                results.sendResponse(req, res, outputJson);
            } else {
                logger.info(`validatePswRstToken - else part is called`);
                req.user = {
                    loginId: token.loginId,
                    resetToken: true
                };
                next();
            }
        });
    } catch (error) {
        logger.error(`validatePswRstToken - catch : ${JSON.stringify(error)}`);
        outputJson.status = constants.httpResponseCodes.failure;
        outputJson.message = constants.messages.authentication.loginUnknownErr;
        results.sendResponse(req, res, outputJson);
    }
};



exports.resetPswToken = (req, res) => {
    logger.info('resetPswToken - called');
    let outputJson = {};
    try {
        loginDbObj.findOne({
            email: req.body.email.toLowerCase(),
            isDeleted: false
        }).exec((err, user) => {
            if (err) {
                logger.error(`resetPswToken - findOne : error : ${JSON.stringify(err)}`);
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.loginUnknownErr;
                results.sendResponse(req, res, outputJson);
            } else if (!user) {
                logger.info(`resetPswToken - findOne : response : user not found`);
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.actNotFoundErr;
                results.sendResponse(req, res, outputJson);
            } else if (!user.isApproved) {
                logger.info(`resetPswToken - findOne : response : user is not approved`);
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.acctApprovedErr;
                results.sendResponse(req, res, outputJson);
            }
            //check if account is blocked or not by SA
            else if (user.isBlocked) {
                logger.info(`resetPswToken - findOne : response : user is blocked`);
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.acctBlockedErr;
                results.sendResponse(req, res, outputJson);
            } else {
                logger.info('resetPswToken - findOne - generatePswResetToken');
                generatePswResetToken(req, res, outputJson, user);
            }
        });
    } catch (error) {
        logger.error(`resetPswToken - catch : ${JSON.stringify(error)}`);
        outputJson.status = constants.httpResponseCodes.failure;
        outputJson.message = constants.messages.authentication.loginUnknownErr;
        results.sendResponse(req, res, outputJson);
    }
};

var generatePswResetToken = (req, res, outputJson, user) => {
    logger.info(`generatePswResetToken - called `);
    try {
        let resetToken = user.generateAuthToken(user.email);
        let currentDate = new Date();
        let token = new pswResetdbObj({
            loginId: user._id,
            token: resetToken,
            expiredOn: new Date(currentDate.getTime() + (24 * 60 * 60 * 1000)).toISOString()
        });
        logger.info(`generatePswResetToken - token : ${JSON.stringify(token)}`);
        token.save((err, save) => {
            if (err) {
                logger.error(`generatePswResetToken - save : error : ${JSON.stringify(err)}`);
                outputJson.status = constants.httpResponseCodes.failure;
                outputJson.message = constants.messages.authentication.loginUnknownErr;
                results.sendResponse(req, res, outputJson);
            } else {
                logger.info(`generatePswResetToken - save : response : token is saved`);
                mail.sendPswResetEmail(req, res, user.email, resetToken, outputJson);
                outputJson.status = constants.httpResponseCodes.success;
                outputJson.message = constants.messages.resetPassword.success;
                results.sendResponse(req, res, outputJson);
            }
        });
    } catch (error) {
        logger.error(`generatePswResetToken - catch : ${JSON.stringify(error)}`);
        outputJson.status = constants.httpResponseCodes.failure;
        outputJson.message = constants.messages.authentication.loginUnknownErr;
        results.sendResponse(req, res, outputJson);
    }
};