'use strict';
import {
    check,
    validationResult,
    oneOf,
    body
} from 'express-validator/check';
import results from "./../../config/results";
import constants from "./../../config/messages";

/*
 * validators.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Framework
 * @description :: validator.js Module dependencies.
 * @Company     :: Trantor Inc.
 * @Date        :: 16 October 2018
 */

exports.loginRequestValidator = [
    check('email')
    .exists().isEmail()
    .withMessage(constants.messages.authentication.emailReqErr),
    check('password')
    .exists()
    .withMessage(constants.messages.authentication.passswordReqErr)
];

exports.registerValidator = [
    check('password').matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,}$/, "i").withMessage(constants.messages.changePassword.passwordRegExErr)
];


exports.logoutRequestValidator = [
    check('apiToken')
    .exists()
    .withMessage(constants.messages.authentication.apiTokenReqErr)
];

exports.changePaswVal = [
    check('currentPassword')
    .exists()
    .withMessage(constants.messages.changePassword.currentPaswReqErr),
    check('newPassword')
    .exists()
    .withMessage(constants.messages.changePassword.newPaswReqErr),
    check('confirmPassword')
    .exists()
    .withMessage(constants.messages.changePassword.confirmPaswReqErr),
    check('newPassword').custom((value, {
        req
    }) => {
        if (value === req.body.currentPassword) {
            throw new Error(constants.messages.changePassword.newOldPaswMatchErr);
        } else if (value !== req.body.confirmPassword) {
            throw new Error(constants.messages.changePassword.cfrmPaswNotMatchedErr);
        } else {
            return true;
        }
    }),
    check('newPassword').isLength({
        min: 8,
        max: 14
    }).withMessage(constants.messages.changePassword.paswLengthErr),
    check('newPassword').matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,}$/, "i").withMessage(constants.messages.changePassword.passwordRegExErr)
];

exports.forgotPaswVal = [
    check('token')
    .exists()
    .withMessage(constants.messages.changePassword.pswResetTokenReqErr),
    check('newPassword')
    .exists()
    .withMessage(constants.messages.changePassword.newPaswReqErr),
    check('confirmPassword')
    .exists()
    .withMessage(constants.messages.changePassword.confirmPaswReqErr),
    check('newPassword').custom((value, {
        req
    }) => {
        if (value !== req.body.confirmPassword) {
            throw new Error(constants.messages.changePassword.cfrmPaswNotMatchedErr);
        } else {
            return true;
        }
    }),
    check('newPassword').isLength({
        min: 8,
        max: 14
    }).withMessage(constants.messages.changePassword.paswLengthErr),
    check('newPassword').matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,}$/, "i").withMessage(constants.messages.changePassword.passwordRegExErr)
];


exports.resetPswTokenVal = [
    check('email')
    .exists().isEmail()
    .withMessage(constants.messages.authentication.usrnameReqErr)
];

exports.inputValidation = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        results.sendArrayResponse(req, res, {
            status: constants.httpResponseCodes.notAcceptable,
            message: errors.array()
        });
    } else {
        next();
    }
};

exports.accountListSchema = [
    check('userId').exists().withMessage(constants.messages.account.userId)
]

exports.accountSchema = [
    check('userId').exists().withMessage(constants.messages.account.userId),
    check('accountId').exists().withMessage(constants.messages.account.accountId)
]

exports.consentSchema = [
    check('consentId').exists().withMessage(constants.messages.consent.consentId)
]
exports.isValidEmail = [
    check('email').isEmail()
    .withMessage(constants.messages.authentication.usrnameReqErr)
];

exports.searchTermReqVal = [
    check('pageNumber').exists().withMessage(constants.messages.user.pageNumberReqErr), 
    check('pageSize').exists().withMessage(constants.messages.user.pageSizeReqErr)
];

exports.inviteValidation = [
    oneOf([
        check('merchantId').exists().withMessage(constants.messages.invite.merchantIdReqErr),
        check('userId').exists().withMessage(constants.messages.account.userId)
    ]),
    body('email').optional().isEmail().withMessage(constants.messages.authentication.usrnameReqErr)
]

exports.transactionSchema = [
    check('userId').exists().withMessage(constants.messages.account.userId),
    check('accountId').exists().withMessage(constants.messages.account.accountId),
    check('pageNumber').exists().withMessage(constants.messages.transaction.pageNumberNotExist),
    check('pageSize').exists().withMessage(constants.messages.transaction.pageSizeNotExist)
]
exports.consentListSchema = [
    check('userId').exists().withMessage(constants.messages.account.userId),
    check('includeExpired').custom((value, {
        req
    }) => {
        if (req.body.hasOwnProperty('includeExpired')) {
            if(typeof value != 'boolean'){
                throw new Error(constants.messages.consent.includeExpired);
            }else{
                return true
            }
        } else {
            return true;
        }
    })
]

exports.validateCmd  =[
    check('cmd').exists().withMessage(constants.messages.authentication.valdCmdReqErr)
] 