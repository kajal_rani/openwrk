const moment = require('moment');
const logger = require('../../config/logger')(module);

exports.convertDateToUTC = (dateString) =>{
    logger.info(`convertDateToUTC - with : ${dateString}`);
    //moment 
    const momentDate = moment(dateString).utc().format();//'YYYY-MM-DDTHH:mm:ss.SSS'
    return momentDate;
}

exports.compareDate = (fromDateString, toDateString) =>{
    logger.info(`compareDate - ${fromDateString} with ${toDateString}`);
    const momentFromDate = moment(fromDateString).utc().valueOf();
    const momentToDate = moment(toDateString).utc().valueOf();
    if(momentFromDate >= momentToDate){
        return false;
    }else{
        return true;
    }
}