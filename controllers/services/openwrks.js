'use strict';
/*
 * consents.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Openwrks consents.js controllers
 * @description :: API/consents/consents.js Module dependencies.
 * @Company     :: Trantor Inc.
 * @Date        :: 8 January 2019
 */
const request = require('request');
module.exports = opts => {
  const defaults = {
    method: opts.method,
    headers: {
      'cache-control': 'no-cache',
      accept: 'application/json',
      'content-type': 'application/json',
    },
    json: true,
    strictSSL: false,
  };
  if (opts.accessToken) {
    defaults.headers['authorization'] = `Bearer ${opts.accessToken}`;
  }
  const options = Object.assign({}, defaults, opts);
  return new Promise((resolve, reject) => {
    request(options, (error, headers, response) => {
      if (error) {
        reject(error);
      } else {
        resolve({response, header: headers.headers});
      }
    });
  });
};
