'use strict';
import allCtrls from "./../config/controllers";
import validators from "./../controllers/validators/validator";
import tokenValidation from "./../controllers/policies/authMdl";
module.exports = function (app, express, router) {
    router.post('/login', validators.loginRequestValidator, validators.inputValidation, allCtrls.auth);
    router.get('/logout', tokenValidation.validateToken, allCtrls.logout);
    router.post('/register', validators.loginRequestValidator, validators.registerValidator, validators.inputValidation, allCtrls.register);
    router.post('/changePassword', validators.changePaswVal, validators.inputValidation, tokenValidation.validateToken, allCtrls.changePassword);
    router.post('/resetPswToken', validators.resetPswTokenVal, validators.inputValidation, allCtrls.resetPswToken);
    router.post('/forgotPassword', validators.forgotPaswVal, validators.inputValidation, allCtrls.validatePswRstToken, allCtrls.forgotPassword);
    app.use("/auth", router);
};