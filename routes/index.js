'use strict';
module.exports = function (app, express, router) {
  /* GET users listing. */
  router.get('/', function (req, res) {
    /*  res.render('index', {
      title: 'Openwrks APIs sample 1.0.0'
    }); */
    res.redirect('/documentations');
  });
  app.use("/",router);
};