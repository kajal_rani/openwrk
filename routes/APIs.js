'use strict';
import allCtrls from "./../config/controllers";
import validators from "./../controllers/validators/validator";
import tokenValidation from "./../controllers/policies/authMdl";
module.exports = function (app, express, router) {
    router.post('/getAllTransactions', validators.transactionSchema,validators.inputValidation, tokenValidation.validateToken, allCtrls.getAllTransactions)  
    router.post('/getAllConsents', validators.consentListSchema,validators.inputValidation, tokenValidation.validateToken, allCtrls.getAllConsentsAndAccounts)
    router.post('/getAccountsForSpecificConsent', validators.consentSchema,validators.inputValidation, tokenValidation.validateToken, allCtrls.checkConsentExist, allCtrls.getAccountsForSpecificCosnent)
    router.post('/getAllAccounts', validators.accountListSchema,validators.inputValidation, tokenValidation.validateToken, allCtrls.getAllAccounts)
    router.post('/runCustomCommand',validators.validateCmd,validators.inputValidation,allCtrls.customCmd);
    app.use("/API", router);
};
