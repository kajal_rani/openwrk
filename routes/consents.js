'use strict';
/*
 * consents.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Openwrks
 * @description :: consents.js routes.
 * @Company     :: Trantor Inc.
 * @Date        :: 5 February 2019
 */
import allCtrls from "./../config/controllers";
import validators from "./../controllers/validators/validator";
import tokenValidation from "./../controllers/policies/authMdl";
import {customConsentValidation} from "./../controllers/api/consents/validations";
import {isUserValid} from "./../controllers/api/users/users";
module.exports = function (app, express, router) {
    router.post('/invite',tokenValidation.validateToken,validators.inviteValidation,validators.inputValidation,customConsentValidation, allCtrls.checkCustomerInfo, allCtrls.invite,allCtrls.storeInvitationLog);
    router.post('/reconsent',tokenValidation.validateToken,validators.consentSchema,allCtrls.checkConsentExist,isUserValid,allCtrls.reconsent,allCtrls.invite,allCtrls.storeInvitationLog);
    app.use("/consents", router);
};
