'use strict';
/*
 * consents.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Openwrks
 * @description :: users.js routes.
 * @Company     :: Trantor Inc.
 * @Date        :: 5 February 2019
 */
import allCtrls from "./../config/controllers";
import validators from "./../controllers/validators/validator";
import tokenValidation from "./../controllers/policies/authMdl";
module.exports = function (app, express, router) {
        router.post('/getAllUsers', tokenValidation.validateToken, validators.searchTermReqVal, validators.inputValidation, allCtrls.searchAllUsers);
        app.use("/users", router);
};
