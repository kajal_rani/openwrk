'use strict';
/*
 * App.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Openwrks
 * @description :: App.js Module dependencies.
 * @Company     :: Trantor Inc.
 * @Date        :: 8 January 2019
 */

/*
	To support older version of ECMA script and also handle latest features 
	of ECMA 6,7 and 8 bable files are intialized here at the top
*/
require('babel-core/register');
require('babel-polyfill');
require("babel-core").transform("code", {
    presets: ["latest"]
});

/*
	List of required modules used in entire application
*/

// Global intialization of promises
global.Promise = require('bluebird');

// Global initialization of lodash library for array operations 
global.__ = require('lodash');

const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');
const cors = require('cors');
const engines = require('consolidate');
const dotenv = require('dotenv');
const expressValidator = require('express-validator');
const fs = require('fs');
const rfs = require('rotating-file-stream');
const helmet = require('helmet');
const expressSession = require('express-session');
const compression = require('compression');
var app = express();
app.use(compression());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// MOrgan logger for logging all the apis on server console durign any service reqquest
app.use(logger(':method :url :response-time ms - :res[content-length] :date[web] :status :remote-addr'));
const logDirectory = path.join(__dirname, 'log')
    // ensure log directory exists 
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory)
    // create a rotating write stream 
var accessLogStream = rfs('access.log', {
    interval: '5d', // rotate daily 
    path: logDirectory
})

app.use(logger('dev'));
app.use(bodyParser.json({
    limit: '50mb'
}));
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
    maxAge:(15 * 60 * 1000)
}));
app.use(cookieParser());
//Response headers security settings
app.use(helmet());
app.disable('x-powered-by');

app.use(express.static(path.join(__dirname,'public')));
dotenv.load();
require("./config/db.js");
app.use(expressValidator());
app.set('trust proxy', 1);
app.use(expressSession({
    secret: 'Openwrks@2019',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: true }
}));
app.use(passport.initialize());
app.use(passport.session());
/* Swagger Configuration start */
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./config/swagger.json');
var options = {
  customJs: '/custom.js'
}; 
app.use('/documentations', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));
/* Swagger Configuration End */
// app.use(function(req, res, next) {
//     for (var item in req.body) {
//       req.sanitize(item).escape();
//     }
//     next();
//   });
// all routes settings to allow cross origin with no cache
app.all('/*', function(request, response, next) {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Headers", "X-Requested-With");
    response.header("Access-Control-Allow-Methods", "GET, POST");
    response.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    response.header('Expires', '-1');
    response.header('Pragma', 'no-cache'); 
    next();
});

/* code to render all route files dynamically */

const router = express.Router();
const customLogger = require('./config/logger')(module);
try {
    fs.readdirSync(path.join(__dirname, 'routes')).map(file => {
        require('./routes/' + file)(app, express, router);        
    });

} catch (error) {
    console.log("error:",error);
    customLogger.info(`routes intialization - Catch : ${error}`);
}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;