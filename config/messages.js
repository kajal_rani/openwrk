/*
 * messages.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: 8 January 2018
 * @description :: This file is used to write out all the response messages of apis
 *
 *
 */
exports.messages = {
    "authentication": {
        "success": "Welcome! Successfully logged in.",
        "usrnameReqErr": "Valid email is required.",
        "usrnameUniqueErr": "Email is already registered.",
        "passswordReqErr": "Valid password is required.",
        "tokenReqErr": "Authorization token is required.",
        "loginUnknownErr": "We regret your inconvenience, something went wrong.",
        "actNotFoundErr": "Couldn't found your account.",
        "apiTokenReqErr": "API authentication token secret is required in header.",
        "apiTokenInvalidErr": "API authentication token secret is invalid.",
        "apiTokenExpiredErr": "API authentication token secret is expired.",
        "logoutMsg": "Thanks! please visit again to use our services.",
        "valTknunknownErr":"We regret your inconvenience, something went wrong while validating api authorization token.",
        "acctApprovedErr":"Your account is not approved by System Administrator, please contact to System Administrator for account approval process.",
        "acctBlockedErr":"Your account services have been blocked! please contact System Administrator for unblocking account services.",
        "warningMsg":"Access to Financial Bureaus systems is only for authorized users and only for permissible purpose access. Unauthorized access is prohibited under the Fair Credit Reporting Act (FCRA) and punishable by $2500 and/or 1 year in Federal prison per occurrence.",
        "emailReqErr": "Email is required.",
        "securityTokenReqErr":"Data Security key is required.",
        "valdCmdReqErr":"Valid CURL cmd is required."
    },
    "register": {
        "success": "Congratulations! Successfully registered.",
        "registerUnknownErr": "We regret your inconvenience, something went wrong.",
    },
    "changePassword":{
        "currentPaswReqErr":"User Current password is required.",
        "newPaswReqErr":"New password is required.",
        "confirmPaswReqErr":"Confirm password is required.",
        "success":"Your password has been changed.",
        "chgPaswUnknownErr": "We regret your inconvenience, something went wrong.",
        "cfrmPaswNotMatchedErr":"Confirm password should be same as new password.",
        "newOldPaswMatchErr":"New password can not be same as Current Password.",
        "paswLengthErr":"8 to 14 characters required",
        "passwordRegExErr":"Password must include one lowercase character, one uppercase character, a number, and a special character.",
        "pswResetTokenReqErr":"Password reset token is required.",
        "wrongCrtPswErr":"Your Current password is incorrect.",
    },
    "resetPassword":{
        "success":"Congratulations! your password reset token has been sent to your email, Token will expire in 24 hours.",
        "invalidRstToken":"Invalid password reset token.",
        "tokenExpiredErr":"Password token is expired please generate new one.",
    },
    "consent" : {
        "notApproved" : "Consent is not approved.",
        "noRecordFound" : "Consent not found.",
        "consentId" : "consentId is required.",
        "includeExpired" : "includeExpired must be true/false boolean."
    },
    "user" : {
        "notExist" : "User is not exist",
        "noRecordFound":"User record not found.",
        "searchTermErr":"Search term for searching users is required.",
        "pageNumberReqErr":"Page number is required.",
        "pageSizeReqErr":"Page size is required.",
    },
    "account" : {
        "userId" : "userId is required.",
        "accountId" : "accountId is required."
    },
    "invite":{
        "invalidUserId":"Invalid 'userId' for Openwrks environment.",
        "requiredParamErr":"Input parameters are required.",
        "userIdReqErr":"userId is required.",
        "firstnameReqErr":"First name is required.",
        "surnameReqErr":"Surname is required.",
        "leadIdReqErr":"leadId is required.",
        "bizIdReqErr":"bizId is required.",
        "businessNameReqErr":"Business Name is required.",
        "allDataErr":"Either provide 'userId' input for old user or provide metdata for new user(merchantId, firstname, surname, businessName, leadId, bizId)",
        "merchantIdReqErr":"Merchant Id is required."
    },
    "transaction":{
        "invalidDateFilter" : "'From Date' must be less then 'To Date'.",
        "pageNumberNotExist" : "pageNumber is required.",
        "pageSizeNotExist" : "pageSize is required."
    }
};

exports.httpResponseCodes = {
    "success": 200,
    "failure": 400,
    "notAcceptable":406,
    "unAuthorizedAccess":401,
    "invalidParameters":422,
    "internalServer":500
}