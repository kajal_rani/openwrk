
const validateUpTo = 30;
const maxLogFileSize = process.env.LOGGING_FILE_SIZE ? process.env.LOGGING_FILE_SIZE :10000000;

const openwrksServices = {
    "invite":"/v1/invite",
    "users":"/v1/users",
    "accounts":"/accounts",
    "transactions":"/transactions",
    "consents":"/consents",
    "reconsent":"/v1/invite/reconsent"
}

const redirectionUrl = "https://boostcapital.co.uk/";


module.exports = {
    validateUpTo:validateUpTo,
    maxLogFileSize : maxLogFileSize,
    openwrksServices:openwrksServices,
    redirectionUrl:redirectionUrl
}