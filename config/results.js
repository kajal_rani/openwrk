'use strict';
/**
 * controller.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Openwrks results module
 * @description :: results.js Module dependencies to send responses from common controller.
 * @Company     :: Trantor Inc.
 * @Date        :: 8 January 2019
 */
const logger = require('./logger')(module);
exports.sendResponse = (req, res, outputJson) => {
    let result = {};
    try {
        result = {
            status: outputJson.status,
            message: outputJson.message,
            error: parseInt(outputJson.status) === 200 ? false : true
        };
        if (outputJson.user) result.user = outputJson.user;
        if (outputJson.warning) result.warning = outputJson.warning;
        if (outputJson.data) result.data = outputJson.data;
        if (outputJson.total) result.total = outputJson.total;
        if (outputJson.pageResults) result.pageResults = outputJson.pageResults;
        if (outputJson.page) result.page = outputJson.page;
        if (outputJson.errorCode) result.errorCode = outputJson.errorCode;
        if (outputJson.code) result.code = outputJson.code;
        if (outputJson._link) result._link = outputJson._link;
        if (outputJson.totalNumberOfRecords) result.totalNumberOfRecords = outputJson.totalNumberOfRecords;
        if (outputJson.pageNumber) result.pageNumber = outputJson.pageNumber;
        if (outputJson.pageSize) result.pageSize = outputJson.pageSize;
        if (outputJson.totalNumberOfPages) result.totalNumberOfPages = outputJson.totalNumberOfPages;
        if(req.body.bizId) result.bizId = req.body.bizId;
        if(req.body.leadId) result.leadId = req.body.leadId;
        logger.info(`sendResponse - success Response : ${JSON.stringify(outputJson)}`);
        return res.status(outputJson.status).jsonp(result);
    } catch (e) {
        logger.info(`Send Response - Catch internal : ${e}`);
    }

};

exports.sendArrayResponse = (req, res, outputJson) => {
    try {
        logger.info(`sendArrayResponse - success Response : ${JSON.stringify(outputJson)}`);
        return res.status(outputJson.status).jsonp({
            status: outputJson.status,
            messages: outputJson.message,
            error: parseInt(outputJson.status) === 200 ? false : true
        });
    } catch (e) {
        logger.info(`Send ArrayResponse - Catch internal : ${e}`);
    }
};


