const winston = require('winston');
const format = require('winston').format;
const logDirectory = './log';
const maxLogFileSize = require('./constants').maxLogFileSize;

const getLabel = function (callingModule) {
    const parts = callingModule.filename.split('/');
    return parts[parts.length - 2] + '/' + parts.pop();
};

module.exports = function (callingModule) {
    if (process.env.NODE_ENV === process.env.LOGGING_ENV || process.env.LOGGING_ENV.toLocaleUpperCase() === 'all') {
        return winston.createLogger({
            format: format.combine(
                format.timestamp({
                    format : 'YYYY-MM-DD HH:mm:ss'
                }),
                format.label({
                    label : getLabel(callingModule)
                }),
                format.printf(
                    info => `${info.timestamp} ${info.level} ${info.label} : ${info.message}`
                )
            ),
            transports: [
                new (require('winston-daily-rotate-file'))({
                    name: 'normal',
                    datePattern: 'YYYY-MM-DD',
                    prepend: true,
                    maxsize: maxLogFileSize,
                    filename: logDirectory + '/.log',
                    maxFiles: '10d',
                    colorize: true,
                    localTime: true
                }),
                new (require('winston-daily-rotate-file'))({
                    name: 'error',
                    datePattern: 'YYYY-MM-DD',
                    prepend: true,
                    maxsize: maxLogFileSize,
                    filename: logDirectory + '/error.log',
                    level: 'error',
                    colorize: true,
                    maxFiles: '10d',
                    localTime: true
                }),
                new (require('winston-daily-rotate-file'))({
                    name: 'info',
                    datePattern: 'YYYY-MM-DD',
                    prepend: true,
                    maxsize: maxLogFileSize,
                    filename: logDirectory + '/info.log',
                    level: 'info',
                    colorize: true,
                    maxFiles: '10d',
                    localTime: true,
                    json: true
                })
            ]
        });
    }else{
        return winston.createLogger({
            transports: [new winston.transports.Console({
                level: 'error'
            })]
        });
    }
}