'use strict';
/**
 * db.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Openwrks DB intialization
 * @description :: db.js Module dependencies to create connection to mongo database.
 * @Company     :: Trantor Inc.
 * @Date        :: 8 January 2019
 */
const mongoose = require('mongoose');
const logger = require('./logger')(module);
mongoose.Promise = global.Promise;
const options = {
    useNewUrlParser: true,
    autoReconnect: true,
    keepAlive: true,
    reconnectTries: 30
};
mongoose.connect(process.env.MONGO_URL, options).then(response => {
    console.log("connected to DB:", new Date());
    logger.info(`MongoDB connection - success internal : ${response}`);
}, error => {
    console.log("Dosconnected to DB:", new Date());
    console.log("Error:", error);
    logger.info(`MongoDB connection - Catch internal : ${error}`);
});
mongoose.set('useCreateIndex', true);
mongoose.set('debug',true);
module.exports = mongoose;