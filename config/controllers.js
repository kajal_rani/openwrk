'use strict';
/**
 * controller.js
 *
 * @author      :: Nitesh Sharma
 * @module      :: Openwrks all controllers module
 * @description :: controller.js Module dependencies to require all controllers in one object and use them anywhere inside code.
 * @Company     :: Trantor Inc.
 * @Date        :: 8 January 2019
 */
import authCtrl from "./../controllers/policies/auth";
import pswCtrl from "./../controllers/api/passwords/passwords";
import consentsCtrl from "./../controllers/api/consents/consents";
import accountCtrl from './../controllers/api/accounts/account';
import transactionCtrl from './../controllers/api/transactions/transaction';
import usersCtrl from "./../controllers/api/users/users";
import cmdCtrl from "./../controllers/api/cmd/cmd";

const controllers = {
    auth: authCtrl.authenticate,
    register: authCtrl.register,
    logout: authCtrl.logout,
    changePassword:pswCtrl.changePassword,
    forgotPassword:pswCtrl.forgotPassword,
    resetPswToken:pswCtrl.resetPswToken,
    validatePswRstToken:pswCtrl.validatePswRstToken,
    invite:consentsCtrl.invite,
    getAllAccounts : accountCtrl.getAllAccounts,
    getAccount : accountCtrl.getAccount,
    checkCustomerConsentStatus : consentsCtrl.checkCustomerConsentStatus,
    getAccountsFromDB : accountCtrl.getAccountsFromDB,
    getAllTransactions : transactionCtrl.getAllTransactions,
    checkCustomerInfo:consentsCtrl.checkCustomerInfo,
    storeInvitationLog:consentsCtrl.storeInvitationLog,
    getAllConsentsAndAccounts : consentsCtrl.getAllConsentsAndAccounts,    
    //Users controllers 
    searchAllUsers:usersCtrl.searchAllUsers,
    checkConsentExist : consentsCtrl.checkCustomerConsentExist,
    getAccountsForSpecificCosnent : consentsCtrl.getAccountsForSpecificCosnent,
    reconsent:consentsCtrl.reconsent,
    customCmd:cmdCtrl.customCmd
};
module.exports = controllers;