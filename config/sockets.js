const io = require("socket.io")();
global.socketCounter = 0;
io.on('connection', function (socket) {
    //console.log("Socket id:",socket.id);
    socketCounter++;
    //console.log("inside socket connection:",socketCounter);
    socket.on('showMessage',function(data){
        //console.log("data:",data);
    });
    socket.emit("retrieveMessage",{status:200,message:"data received"});
});

io.on('disconnect', function () {
    socketCounter--;
    //console.log("inside socket disconnect:",socketCounter);
});

module.exports = io;
